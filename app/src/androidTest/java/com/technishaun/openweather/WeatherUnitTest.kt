package com.technishaun.openweather

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.technishaun.openweather.objects.request.RequestCitiesWeather
import com.technishaun.openweather.objects.request.RequestCityWeather
import com.technishaun.openweather.usecases.WeatherUseCase
import org.junit.Assert
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class WeatherUnitTest: KoinComponent {

    val mWeatherUseCase by inject<WeatherUseCase>()

    private val lock: CountDownLatch = CountDownLatch(1)

    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("com.technishaun.openweather", appContext.packageName)
    }

    @Test
    fun testIfCityExistsOnDatabaseFromFetching() {
        val request = RequestCitiesWeather(listOf(1701668, 3067696, 1835848))

        mWeatherUseCase.getCitiesWeather(
            request,
            onSuccess = {
                var allTestsPassed = false
                val citiesRequired = listOf("Manila", "Prague", "Seoul")

                for (tblWeather in it) {
                    allTestsPassed = citiesRequired.contains(tblWeather.city)

                    if (!allTestsPassed) break
                }

                Assert.assertTrue(allTestsPassed)
                lock.countDown()
            },
            onError = {
                Assert.assertTrue(false)
                lock.countDown()
            }
        )

        lock.await(10, TimeUnit.SECONDS)
    }

    @Test
    fun testMinAndMaxTemperatureFormattingFromAPI() {
        val request = RequestCityWeather(1701668)
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext

        mWeatherUseCase.getCityWeather(
            request,
            onSuccess = {
                val temperature = appContext.resources.getString(
                    R.string.temperature_low_high,
                    it.highestTemperature.toInt(),
                    it.lowestTemperature.toInt()
                )

                // split string using /
                val highAndLowTemperatures = temperature.split("/")
                var allTestsPassed = false

                for (highAndLowTemperature in highAndLowTemperatures) {
                    val temperature = highAndLowTemperature.split(" ")[1]
                    allTestsPassed = !temperature.contains(".")

                    if (!allTestsPassed) break
                }

                Assert.assertTrue(allTestsPassed)
                lock.countDown()
            },
            onError = {
                Assert.assertTrue(false)
                lock.countDown()
            }
        )

        lock.await(10, TimeUnit.SECONDS)
    }

    @Test
    fun testCurrentTemperatureFormattingFromAPI() {
        val request = RequestCityWeather(1701668)
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext

        mWeatherUseCase.getCityWeather(
            request,
            onSuccess = {
                val temperature = appContext.resources.getString(R.string.temperature, it.temperature)
                Assert.assertTrue(temperature.split(".")[1].length == 1)
                lock.countDown()
            },
            onError = {
                Assert.assertTrue(false)
                lock.countDown()
            }
        )

        lock.await(10, TimeUnit.SECONDS)
    }

    @Test
    fun testFavoriteCity() {
        // fetch cities locally
        val request = RequestCitiesWeather(listOf(1701668, 3067696, 1835848))
        mWeatherUseCase.getCitiesWeather(
            request,
            forceRemote = true,
            onSuccess = {
                val initialManilaValue = it.find { city -> city.id == 1701668.toLong() }
                if (initialManilaValue == null) {
                    Assert.assertTrue("Manila is not part of the database", false)
                    lock.countDown()
                    return@getCitiesWeather
                } else if (initialManilaValue.favorite) {
                    Assert.assertTrue("Database didn't refresh", false)

                    lock.countDown()
                    return@getCitiesWeather
                }

                // update value to favorite
                initialManilaValue.favorite = true

                // store data to database
                mWeatherUseCase.updateCityItem(
                    initialManilaValue,
                    onSuccess = {
                        // fetch database locally if favorite was applied
                        mWeatherUseCase.getCitiesWeather(
                            request,
                            forceRemote = false,
                            onSuccess = success@{ weatherList ->
                                val updatedManilaValue =
                                    weatherList.find { city -> city.id == 1701668.toLong() }
                                if (updatedManilaValue == null) {
                                    Assert.assertTrue("Manila is missing in the database", false)
                                    lock.countDown()
                                    return@success
                                }

                                Assert.assertTrue("Manila favorite updated", updatedManilaValue.favorite)
                                lock.countDown()
                            },
                            onError = { error ->
                                Assert.assertTrue(error.message!!, false)
                                lock.countDown()
                            }
                        )
                    }
                )
            },
            onError = {
                Assert.assertTrue(it.message!!, false)
                lock.countDown()
            }
        )

        lock.await(10, TimeUnit.SECONDS)
        Assert.assertTrue(false)
    }
}