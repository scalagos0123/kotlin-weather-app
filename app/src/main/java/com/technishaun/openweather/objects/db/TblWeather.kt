package com.technishaun.openweather.objects.db

import android.os.Parcel
import android.os.Parcelable
import com.technishaun.openweather.objects.items.responsecityweather.City
import com.technishaun.openweather.objects.response.ResponseCitiesWeather
import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id

@Entity
data class TblWeather(
    @Id(assignable = true) var id: Long = 0,
    var city: String? = null,
    var temperature: Double = 0.00,
    var lowestTemperature: Double = 0.00,
    var highestTemperature: Double = 0.00,
    var weatherType: String? = null,
    var favorite: Boolean = false
) : Parcelable {
    constructor(source: Parcel) : this(
        source.readLong(),
        source.readString(),
        source.readDouble(),
        source.readDouble(),
        source.readDouble(),
        source.readString(),
        1 == source.readInt()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeLong(id)
        writeString(city)
        writeDouble(temperature)
        writeDouble(lowestTemperature)
        writeDouble(highestTemperature)
        writeString(weatherType)
        writeInt((if (favorite) 1 else 0))
    }

    companion object {
        fun toTblWeather(data: City): TblWeather =
            TblWeather(
                id = data.id.toLong(),
                city = data.name,
                temperature = data.temperature.temp,
                lowestTemperature = data.temperature.tempMin,
                highestTemperature = data.temperature.tempMax,
                weatherType = data.weather[0].main
            )

        fun toTblWeather(list: List<City>): List<TblWeather> = kotlin.run {
            val convertedCities = mutableListOf<TblWeather>()
            list.forEach {
                convertedCities.add(toTblWeather(it))
            }

            convertedCities.toList()
        }

        @JvmField
        val CREATOR: Parcelable.Creator<TblWeather> = object : Parcelable.Creator<TblWeather> {
            override fun createFromParcel(source: Parcel): TblWeather = TblWeather(source)
            override fun newArray(size: Int): Array<TblWeather?> = arrayOfNulls(size)
        }
    }
}