package com.technishaun.openweather.objects.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.technishaun.openweather.objects.db.TblWeather
import com.technishaun.openweather.objects.request.RequestCitiesWeather
import com.technishaun.openweather.usecases.WeatherUseCase
import org.koin.core.KoinComponent
import org.koin.core.inject

class ViewModelCityList : ViewModel(), KoinComponent {

    private val mWeatherUseCase by inject<WeatherUseCase>()

    val cityList = MutableLiveData<MutableList<TblWeather>>(mutableListOf())
    val loadingState = MutableLiveData(LoadingState.IDLE)
    val errorMessages = MutableLiveData<String>()

    enum class LoadingState { LOADING, IDLE }

    fun loadCities(forceRemote: Boolean = false) {
        // switch to loading state to let the UI know
        loadingState.value = LoadingState.LOADING
        errorMessages.value = null

        // perform initial weather request
        val request = RequestCitiesWeather(listOf(1701668, 3067696, 1835848))
        mWeatherUseCase.getCitiesWeather(
            request,
            forceRemote = forceRemote,
            onSuccess = { data: List<TblWeather> ->
                cityList.value = data.toMutableList()

                // switch to idle state
                loadingState.value = LoadingState.IDLE
            },
            onError = { error: Throwable ->
                errorMessages.value = error.message

                // switch to idle state
                loadingState.value = LoadingState.IDLE
            }
        )
    }
}