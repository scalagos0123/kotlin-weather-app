package com.technishaun.openweather.objects.request

import com.technishaun.openweather.objects.base.BaseRequest

class RequestCityWeather(
    val cityId: Int
): BaseRequest {
    override val data: HashMap<String, String>
        get() = HashMap<String, String>().apply {
            put(KEY_CITY_ID, cityId.toString())
        }

    companion object {
        const val KEY_CITY_ID = "id"
    }
}