package com.technishaun.openweather.objects.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.technishaun.openweather.objects.db.TblWeather
import com.technishaun.openweather.objects.items.responsecityweather.City
import com.technishaun.openweather.objects.request.RequestCityWeather
import com.technishaun.openweather.usecases.WeatherUseCase
import org.koin.core.KoinComponent
import org.koin.core.inject

class ViewModelCityDetails: ViewModel(), KoinComponent {

    private val mWeatherUseCase by inject<WeatherUseCase>()

    val cityDataOnDatabase = MutableLiveData<TblWeather>()
    val loadingState = MutableLiveData<LoadingState>(LoadingState.IDLE)
    val errorMessage = MutableLiveData<String>()

    enum class LoadingState { LOADING, IDLE }

    fun getWeatherDataFromCityId(cityId: Long) {
        loadingState.value = LoadingState.LOADING
        errorMessage.value = null

        val request = RequestCityWeather(cityId.toInt())
        mWeatherUseCase.getCityWeather(
            request,
            onSuccess = { data: TblWeather ->
                loadingState.value = LoadingState.IDLE
                cityDataOnDatabase.value = data
            },
            onError = { error: Throwable ->
                errorMessage.value = error.message

                // switch to idle state
                loadingState.value = LoadingState.IDLE
            }
        )
    }

    fun updateCityData(city: TblWeather) {
        // update database
        mWeatherUseCase.updateCityItem(
            city,
            onSuccess = {  }
        )
    }
}