package com.technishaun.openweather.objects.base

interface BaseRequest {
    val data: HashMap<String, String>
}