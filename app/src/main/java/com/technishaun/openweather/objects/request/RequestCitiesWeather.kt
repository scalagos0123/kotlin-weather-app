package com.technishaun.openweather.objects.request

import com.technishaun.openweather.objects.base.BaseRequest
import java.lang.StringBuilder

class RequestCitiesWeather(
    val cityIds: List<Int>
): BaseRequest {
    override val data: HashMap<String, String>
        get() = HashMap<String, String>().apply {
            val stringBuilder = StringBuilder()
            cityIds.forEach {
                stringBuilder.append("$it,")
            }

            stringBuilder.removeSuffix(",")

            put(KEY_CITY_IDS, stringBuilder.toString())
        }

    companion object {
        const val KEY_CITY_IDS = "id"
    }
}