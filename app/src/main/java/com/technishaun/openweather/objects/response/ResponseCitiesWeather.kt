package com.technishaun.openweather.objects.response


import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable
import com.technishaun.openweather.objects.items.responsecityweather.City

@Parcelize
data class ResponseCitiesWeather(
    @SerializedName("cnt")
    val cnt: Int,
    @SerializedName("list")
    val list: List<City>
): Parcelable