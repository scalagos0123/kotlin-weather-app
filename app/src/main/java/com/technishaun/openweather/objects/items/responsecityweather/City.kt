package com.technishaun.openweather.objects.items.responsecityweather


import android.os.Parcel
import com.google.gson.annotations.SerializedName
import android.os.Parcelable

data class City(
    @SerializedName("id")
    val id: Int,
    @SerializedName("main")
    val temperature: Temperature,
    @SerializedName("name")
    val name: String,
    @SerializedName("weather")
    val weather: List<Weather>
) : Parcelable {
    constructor(source: Parcel) : this(
        source.readInt(),
        source.readParcelable<Temperature>(Temperature::class.java.classLoader)!!,
        source.readString()!!,
        source.createTypedArrayList(Weather.CREATOR)!!
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeInt(id)
        writeParcelable(temperature, 0)
        writeString(name)
        writeTypedList(weather)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<City> = object : Parcelable.Creator<City> {
            override fun createFromParcel(source: Parcel): City = City(source)
            override fun newArray(size: Int): Array<City?> = arrayOfNulls(size)
        }
    }
}