package com.technishaun.openweather.usecases

import com.technishaun.openweather.objects.db.TblWeather
import com.technishaun.openweather.objects.items.responsecityweather.City
import com.technishaun.openweather.objects.request.RequestCitiesWeather
import com.technishaun.openweather.objects.request.RequestCityWeather
import com.technishaun.openweather.objects.response.ResponseCitiesWeather
import com.technishaun.openweather.repositories.local.LocalRepository
import com.technishaun.openweather.repositories.remote.RemoteRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class WeatherUseCase(
    private val localRepository: LocalRepository,
    private val remoteRepository: RemoteRepository
) {
    fun getCitiesWeather(
        request: RequestCitiesWeather,
        forceRemote: Boolean = false,
        onSuccess: (data: List<TblWeather>) -> Unit,
        onError: ((error: Throwable) -> Unit)? = null
    ): Disposable {
        lateinit var response: ResponseCitiesWeather

        return if (forceRemote)
            remoteRepository.getCitiesWeather(request)
            .flatMap {
                response = it
                localRepository.removeData()
            }
            .flatMap { localRepository.storeCities(response.list) }
            .flatMap { localRepository.getCities(request.cityIds.map { it.toLong() }) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onError = { onError?.invoke(it) },
                onSuccess = onSuccess
            )
        else
            localRepository.getCities(request.cityIds.map { it.toLong() })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onError = { onError?.invoke(it) },
                    onSuccess = onSuccess
        )

    }

    fun getCityWeather(
        request: RequestCityWeather,
        onSuccess: (data: TblWeather) -> Unit,
        onError: ((error: Throwable) -> Unit)? = null
    ): Disposable {
        return remoteRepository.getCityWeather(request)
            .flatMap { localRepository.storeCity(it) }
            .flatMap { localRepository.getCity(request.cityId.toLong()) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onError = { onError?.invoke(it) },
                onSuccess = onSuccess
            )
    }

    fun updateCityItem(
        item: TblWeather,
        onSuccess: (data: Boolean) -> Unit,
        onError: ((error: Throwable) -> Unit)? = null
    ): Disposable =
        localRepository.updateCityWeatherItem(item)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onError = { onError?.invoke(it) },
                onSuccess = onSuccess
            )
}