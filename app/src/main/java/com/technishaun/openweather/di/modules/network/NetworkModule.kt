package com.technishaun.openweather.di.modules.network

import com.technishaun.openweather.BuildConfig
import com.technishaun.openweather.di.modules.network.interceptors.GeneralInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory

class NetworkModule {

    val modules = module {
        single<ApiService> {
            val okHttpClient = OkHttpClient.Builder().apply {
                addInterceptor(GeneralInterceptor())
                addInterceptor(HttpLoggingInterceptor().also { it.level = HttpLoggingInterceptor.Level.BODY })
            }.build()

            val retrofit = Retrofit.Builder().apply {
                client(okHttpClient)
                baseUrl(BuildConfig.API_WEATHER_HOST)
                addConverterFactory(GsonConverterFactory.create())
                addConverterFactory(ScalarsConverterFactory.create())
                addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            }.build()

            retrofit.create(ApiService::class.java)
        }
    }
}