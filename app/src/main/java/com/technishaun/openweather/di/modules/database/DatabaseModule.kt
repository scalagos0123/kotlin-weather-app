package com.technishaun.openweather.di.modules.database

import android.app.Application
import android.content.Context
import com.technishaun.openweather.objects.db.MyObjectBox
import io.objectbox.BoxStore
import org.koin.dsl.module

class DatabaseModule {
    val modules = module {
        single<BoxStore> {
            MyObjectBox.builder().apply {
                androidContext(get<Application>())
            }.build()
        }
    }
}