package com.technishaun.openweather.di.modules.repository

import com.technishaun.openweather.repositories.local.LocalRepository
import com.technishaun.openweather.repositories.remote.RemoteRepository
import org.koin.dsl.module

class RepositoryModule {
    val modules = module {
        single { LocalRepository(get()) }
        single { RemoteRepository(get()) }
    }
}