package com.technishaun.openweather.di.modules.network.interceptors

import com.technishaun.openweather.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response

class GeneralInterceptor: Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val rebuiltUrlWithAppId = chain.request().url().newBuilder().apply {
            addQueryParameter("appid", BuildConfig.API_WEATHER_KEY)
            addQueryParameter("units", "metric")
        }.build()

        val request = chain.request().newBuilder().apply {
            url(rebuiltUrlWithAppId)
        }.build()

        return chain.proceed(request)
    }
}