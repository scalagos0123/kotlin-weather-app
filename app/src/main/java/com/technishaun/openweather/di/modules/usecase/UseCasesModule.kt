package com.technishaun.openweather.di.modules.usecase

import com.technishaun.openweather.usecases.WeatherUseCase
import org.koin.dsl.module

class UseCasesModule {
    val modules = module {
        single { WeatherUseCase(get(), get()) }
    }
}