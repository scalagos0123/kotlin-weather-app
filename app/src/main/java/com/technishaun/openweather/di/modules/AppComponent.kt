package com.technishaun.openweather.di.modules

import com.technishaun.openweather.di.modules.database.DatabaseModule
import com.technishaun.openweather.di.modules.network.NetworkModule
import com.technishaun.openweather.di.modules.repository.RepositoryModule
import com.technishaun.openweather.di.modules.usecase.UseCasesModule
import org.koin.core.module.Module

class AppComponent {
    val components = listOf<Module>(
        DatabaseModule().modules,
        NetworkModule().modules,
        RepositoryModule().modules,
        UseCasesModule().modules
    )
}