package com.technishaun.openweather.repositories.local

import com.technishaun.openweather.objects.db.TblWeather
import com.technishaun.openweather.objects.db.TblWeather_
import com.technishaun.openweather.objects.items.responsecityweather.City
import io.objectbox.Box
import io.objectbox.BoxStore
import io.objectbox.kotlin.boxFor
import io.objectbox.kotlin.query
import io.reactivex.Single

class LocalRepository(
    private val database: BoxStore
) {
    fun storeCities(cities: List<City>): Single<Boolean> = Single.fromCallable {
        val cityTable: Box<TblWeather> = database.boxFor()
        cityTable.put(TblWeather.toTblWeather(cities))
        true
    }

    fun getCities(cityIds: List<Long> = listOf()): Single<List<TblWeather>> = Single.fromCallable {
        val cityTable: Box<TblWeather> = database.boxFor()

        if (cityIds.isEmpty())
            cityTable.all
        else
            cityTable.get(cityIds)
    }

    fun storeCity(city: City): Single<Boolean> = Single.fromCallable {
        val cityTable: Box<TblWeather> = database.boxFor()
        // check if city already exists in the database
        try {
            val cityFromDatabase = cityTable[city.id.toLong()]
            val updatedCityData = TblWeather.toTblWeather(city)

            updatedCityData.id = cityFromDatabase.id
            updatedCityData.favorite = cityFromDatabase.favorite

            cityTable.put(updatedCityData)
        } catch (e: NullPointerException) {
            cityTable.put(TblWeather.toTblWeather(city))
        }

        true
    }

    fun getCity(cityId: Long): Single<TblWeather> = Single.fromCallable {
        val cityTable: Box<TblWeather> = database.boxFor()
        cityTable
            .query {
                equal(TblWeather_.id, cityId)
            }
            .findFirst()
    }

    fun updateCityWeatherItem(weather: TblWeather): Single<Boolean> = Single.fromCallable {
        val cityTable: Box<TblWeather> = database.boxFor()
        cityTable.put(weather)
        true
    }

    fun removeData(): Single<Boolean> = Single.fromCallable {
        val cityTable: Box<TblWeather> = database.boxFor()
        cityTable.removeAll()
        true
    }
}