package com.technishaun.openweather.repositories.remote

import com.technishaun.openweather.di.modules.network.ApiService
import com.technishaun.openweather.objects.items.responsecityweather.City
import com.technishaun.openweather.objects.request.RequestCitiesWeather
import com.technishaun.openweather.objects.request.RequestCityWeather
import com.technishaun.openweather.objects.response.ResponseCitiesWeather
import io.reactivex.Single

class RemoteRepository(
    private val apiService: ApiService
) {
    fun getCitiesWeather(request: RequestCitiesWeather): Single<ResponseCitiesWeather> =
        apiService.getCitiesWeather(request.data)

    fun getCityWeather(request: RequestCityWeather): Single<City> =
        apiService.getCityWeather(request.data)
}