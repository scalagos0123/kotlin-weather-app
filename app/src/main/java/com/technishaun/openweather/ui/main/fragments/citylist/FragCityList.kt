package com.technishaun.openweather.ui.main.fragments.citylist

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.technishaun.openweather.R
import com.technishaun.openweather.objects.db.TblWeather
import com.technishaun.openweather.objects.viewmodels.ViewModelCityList
import com.technishaun.openweather.ui.main.fragments.citylist.adapteritems.ItemCity
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.frag_citylist.*
import timber.log.Timber

class FragCityList : Fragment() {

    private val mCityViewModel by activityViewModels<ViewModelCityList>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.frag_citylist, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        srl_citylist_root.setOnRefreshListener(mOnRefresh)

        // setup recyclerview
        initializeRecyclerView()

        // subscribe to city data and loading state
        mCityViewModel.apply {
            loadingState.observe(viewLifecycleOwner, mLoadingStateObserver)
            cityList.observe(viewLifecycleOwner, mCityListObserver)
            errorMessages.observe(viewLifecycleOwner, mLoadingError)
        }
    }

    override fun onResume() {
        super.onResume()

        // determine city list if there is data
        if (mCityViewModel.cityList.value!!.isEmpty()) {
            mCityViewModel.loadCities(forceRemote = true)
            return
        }

        mCityViewModel.loadCities()
    }

    private fun initializeRecyclerView() {
        recv_city_list.apply {
            adapter = GroupAdapter<GroupieViewHolder>()
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        }
    }

    private val mOnRefresh = SwipeRefreshLayout.OnRefreshListener {
        mCityViewModel.loadCities(forceRemote = true)
    }

    private val mCityClicked = { item: ItemCity ->
        // navigate to city details
        findNavController().navigate(FragCityListDirections.actionFragCityListToFragCityDetails(item.item.id))
    }

    private val mLoadingStateObserver = Observer<ViewModelCityList.LoadingState> {
        when (it) {
            ViewModelCityList.LoadingState.LOADING -> srl_citylist_root.isRefreshing = true
            else -> srl_citylist_root.isRefreshing = false
        }
    }

    private val mCityListObserver = Observer<List<TblWeather>> {
        // convert data to list items
        val convertedWeatherItems = mutableListOf<ItemCity>()
        it.forEach { weather -> convertedWeatherItems.add(ItemCity(weather, mCityClicked)) }

        (recv_city_list.adapter as GroupAdapter<*>).apply {
            clear()
            addAll(convertedWeatherItems.toList())
            notifyDataSetChanged()
        }
    }

    private val mLoadingError = Observer<String> {
        it?.let { error ->
            Toast.makeText(requireContext(), error, Toast.LENGTH_LONG).show()
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = FragCityList()
    }
}