package com.technishaun.openweather.ui.main.fragments.citylist.adapteritems

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.technishaun.openweather.R
import com.technishaun.openweather.objects.db.TblWeather
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item

class ItemCity(
    val item: TblWeather,
    private var listener: ((item: ItemCity) -> Unit)? = null
): Item<GroupieViewHolder>() {

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.itemView.apply {
            findViewById<TextView>(R.id.txtv_citylist_cityname).text = item.city
            findViewById<TextView>(R.id.txtv_citylist_temperature).text = context.resources.getString(R.string.temperature, item.temperature)
            findViewById<TextView>(R.id.txtv_citylist_weathertype).text = item.weatherType
            findViewById<ImageView>(R.id.imgv_citylist_favorite).visibility = if (item.favorite) View.VISIBLE else View.INVISIBLE
            findViewById<ConstraintLayout>(R.id.cl_citylist_root).setBackgroundColor(when {
                item.temperature <= 0 -> ContextCompat.getColor(context, R.color.temperatureFreezing)
                item.temperature > 0 && item.temperature <= 15 -> ContextCompat.getColor(context, R.color.temperatureCold)
                item.temperature > 15 && item.temperature <= 30 -> ContextCompat.getColor(context, R.color.temperatureWarm)
                else -> ContextCompat.getColor(context, R.color.temperatureHot)
            })

            setOnClickListener { listener?.invoke(this@ItemCity) }
        }
    }

    override fun unbind(viewHolder: GroupieViewHolder) {
        super.unbind(viewHolder)
        viewHolder.itemView.setOnClickListener(null)
    }

    override fun getLayout(): Int = R.layout.row_citylist_city
}