package com.technishaun.openweather.ui.main.fragments.citydetails

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.technishaun.openweather.R
import com.technishaun.openweather.objects.db.TblWeather
import com.technishaun.openweather.objects.items.responsecityweather.City
import com.technishaun.openweather.objects.viewmodels.ViewModelCityDetails
import com.technishaun.openweather.objects.viewmodels.ViewModelCityList

class FragCityDetails : Fragment() {

    private val mCityViewModel by activityViewModels<ViewModelCityDetails>()
    private val mDetailsArguments by navArgs<FragCityDetailsArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.frag_city_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mCityViewModel.apply {
            cityDataOnDatabase.observe(viewLifecycleOwner, mCityObserver)
            errorMessage.observe(viewLifecycleOwner, mLoadingError)
            getWeatherDataFromCityId(mDetailsArguments.cityId)
        }
    }

    private val mLoadingError = Observer<String> {
        it?.let { error ->
            Toast.makeText(requireContext(), error, Toast.LENGTH_LONG).show()
        }
    }

    private val mCityObserver = Observer<TblWeather> {
        view?.apply {
            findViewById<TextView>(R.id.txtv_citydetails_name).text = it.city
            findViewById<TextView>(R.id.txtv_citydetails_temperature).text = context.resources.getString(
                R.string.temperature,
                it.temperature
            )

            findViewById<TextView>(R.id.txtv_citydetails_low_high_temperatures).text = context.resources.getString(
                R.string.temperature_low_high,
                it.highestTemperature.toInt(),
                it.lowestTemperature.toInt()
            )

            findViewById<TextView>(R.id.txtv_citydetails_weathertype).text = it.weatherType

            findViewById<ImageView>(R.id.imgv_citydetails_favorite).apply {
                isSelected = it.favorite
                setOnClickListener { view ->
                    view.isSelected = !view.isSelected

                    it.favorite = view.isSelected
                    mCityViewModel.updateCityData(it)
                }
            }
        }
    }
}