package com.technishaun.openweather

import android.app.Application
import com.technishaun.openweather.di.modules.AppComponent
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class AppContext: Application() {
    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
        initializeComponents()
    }

    private fun initializeComponents() {
        startKoin {
            androidContext(this@AppContext)
            modules(AppComponent().components)
        }
    }
}